import React from "react";
import proudctImg from "../../Assets/images/ice-tube1.jpg";
// import { motion } from "framer-motion";
import "../../styles/product-card.css";
import { Col } from "reactstrap";

const ProductCard = () => {
  return (
    <Col lg="3" md="4">
      <div className="product__item">
        <div className="product__img">
          <img src={proudctImg} alt="ice tube machine" />
        </div>
        <div className="p-2">
          <h3 className="product__name">Snow Ice Maker</h3>
          <span className="text-center d-block">Ice Machine</span>
        </div>
        <div className="product__card-bottom d-flex align-items-center justify-content-between p-2">
          <span className="price">$193</span>
          <span>
            <i class="ri-add-line"></i>
          </span>
        </div>
      </div>
    </Col>
  );
};

export default ProductCard;
