import Layout from "./components/Layout/Layout";
import "../src/App.css";

function App() {
  return <Layout />;
}

export default App;
